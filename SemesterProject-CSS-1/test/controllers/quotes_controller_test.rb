require 'test_helper'

class QuotesControllerTest < ActionController::TestCase
  setup do
    @quote = quotes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:quotes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create quote" do
    assert_difference('Quote.count') do
      post :create, quote: { customer: @quote.customer, employee: @quote.employee, interest_rate: @quote.interest_rate, inventory: @quote.inventory, number_payment: @quote.number_payment, payment_amount: @quote.payment_amount, quote_status: @quote.quote_status, term: @quote.term, vehicle_total_price: @quote.vehicle_total_price }
    end

    assert_redirected_to quote_path(assigns(:quote))
  end

  test "should show quote" do
    get :show, id: @quote
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @quote
    assert_response :success
  end

  test "should update quote" do
    patch :update, id: @quote, quote: { customer: @quote.customer, employee: @quote.employee, interest_rate: @quote.interest_rate, inventory: @quote.inventory, number_payment: @quote.number_payment, payment_amount: @quote.payment_amount, quote_status: @quote.quote_status, term: @quote.term, vehicle_total_price: @quote.vehicle_total_price }
    assert_redirected_to quote_path(assigns(:quote))
  end

  test "should destroy quote" do
    assert_difference('Quote.count', -1) do
      delete :destroy, id: @quote
    end

    assert_redirected_to quotes_path
  end
end
