json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :make, :model, :color, :year, :vin, :wholesale_price
  json.url inventory_url(inventory, format: :json)
end
