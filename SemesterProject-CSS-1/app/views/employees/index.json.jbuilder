json.array!(@employees) do |employee|
  json.extract! employee, :id, :first_name, :middle_initial, :last_name, :employee_class
  json.url employee_url(employee, format: :json)
end
