json.array!(@sales) do |sale|
  json.extract! sale, :id, :quote, :dealer_price, :sales_tax, :sale_total_amount, :sale_date
  json.url sale_url(sale, format: :json)
end
