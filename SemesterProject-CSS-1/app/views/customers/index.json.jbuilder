json.array!(@customers) do |customer|
  json.extract! customer, :id, :employee_rep, :first_name, :middle_initial, :last_name, :customer_status, :customer_address, :customer_city, :customer_zip, :customer_state
  json.url customer_url(customer, format: :json)
end
