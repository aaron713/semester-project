json.extract! @customer, :id, :employee_rep, :first_name, :middle_initial, :last_name, :customer_status, :customer_address, :customer_city, :customer_zip, :customer_state, :created_at, :updated_at
