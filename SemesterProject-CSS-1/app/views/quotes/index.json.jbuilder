json.array!(@quotes) do |quote|
  json.extract! quote, :id, :employee, :customer, :inventory, :term, :interest_rate, :quote_status, :vehicle_total_price, :number_payment, :payment_amount
  json.url quote_url(quote, format: :json)
end
