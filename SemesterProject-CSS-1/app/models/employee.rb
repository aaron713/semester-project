class Employee < ActiveRecord::Base
  has_many :quotes
  has_many :customers

  def full_name
    "#{first_name} #{middle_initial} #{last_name}"
  end

  def full_name_quote
    "#{first_name} #{middle_initial} #{last_name} || #{employee_class}"
  end

end
