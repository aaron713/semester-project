class Quote < ActiveRecord::Base
  belongs_to :employee
  belongs_to :sale
  belongs_to :inventory

  def quote_info
    "#{customer} #{term} #{interest_rate} #{inventory}"
  end

end
