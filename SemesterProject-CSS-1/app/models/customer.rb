class Customer < ActiveRecord::Base
  belongs_to :employee
  has_many :sales

  def cust_full_name
    "#{first_name} #{middle_initial} #{last_name}"
  end

  def self.search(search)
    where ['last_name LIKE ? OR first_name LIKE ?', "%#{search}%", "%#{search}%" ]
  end
end
