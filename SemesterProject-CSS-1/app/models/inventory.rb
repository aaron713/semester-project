class Inventory < ActiveRecord::Base
  has_many :quote

  def vehicle_info
    "#{make} || #{model} || #{color} || #{year}"
  end

  def self.search(search)
    where ['model LIKE ? OR color LIKE ? OR vin LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%"]
  end
end
