class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :first_name
      t.string :middle_initial
      t.string :last_name
      t.string :employee_class

      t.timestamps null: false
    end
  end
end
