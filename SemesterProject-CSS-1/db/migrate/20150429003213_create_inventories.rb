class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.string :make
      t.string :model
      t.string :color
      t.integer :year
      t.string :vin
      t.decimal :wholesale_price

      t.timestamps null: false
    end
  end
end
