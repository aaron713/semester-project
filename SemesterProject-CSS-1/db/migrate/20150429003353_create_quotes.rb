class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :employee
      t.integer :customer
      t.integer :inventory
      t.integer :term
      t.decimal :interest_rate
      t.string :quote_status
      t.decimal :vehicle_total_price
      t.decimal :number_payment
      t.decimal :payment_amount

      t.timestamps null: false
    end
  end
end
