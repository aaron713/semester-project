class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :employee_rep
      t.string :first_name
      t.string :middle_initial
      t.string :last_name
      t.string :customer_status
      t.string :customer_address
      t.string :customer_city
      t.integer :customer_zip
      t.string :customer_state

      t.timestamps null: false
    end
  end
end
