class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.string :quote
      t.decimal :dealer_price
      t.decimal :sales_tax
      t.decimal :sale_total_amount
      t.date :sale_date

      t.timestamps null: false
    end
  end
end
