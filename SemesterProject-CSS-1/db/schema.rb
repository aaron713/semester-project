# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150429003619) do

  create_table "customers", force: :cascade do |t|
    t.string   "employee_rep"
    t.string   "first_name"
    t.string   "middle_initial"
    t.string   "last_name"
    t.string   "customer_status"
    t.string   "customer_address"
    t.string   "customer_city"
    t.integer  "customer_zip"
    t.string   "customer_state"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string   "first_name"
    t.string   "middle_initial"
    t.string   "last_name"
    t.string   "employee_class"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "inventories", force: :cascade do |t|
    t.string   "make"
    t.string   "model"
    t.string   "color"
    t.integer  "year"
    t.string   "vin"
    t.decimal  "wholesale_price"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "quotes", force: :cascade do |t|
    t.integer  "employee"
    t.integer  "customer"
    t.integer  "inventory"
    t.integer  "term"
    t.decimal  "interest_rate"
    t.string   "quote_status"
    t.decimal  "vehicle_total_price"
    t.decimal  "number_payment"
    t.decimal  "payment_amount"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "sales", force: :cascade do |t|
    t.string   "quote"
    t.decimal  "dealer_price"
    t.decimal  "sales_tax"
    t.decimal  "sale_total_amount"
    t.date     "sale_date"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

end
