# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Inventory.delete_all
Employee.delete_all
Customer.delete_all

Inventory.create(make: "Ford",model: "Fiesta",color: "Red",year: "2010",vin: "WEFW5A61W5E",wholesale_price: "18000")
Inventory.create(make: "Toyota",model: "Prius",color: "White",year: "2011",vin: "WEADCX581",wholesale_price: "20000")
Inventory.create(make: "Jeep",model: "Montero",color: "Orange",year: "2014",vin: "S5E16AE89F",wholesale_price: "24500")
Inventory.create(make: "Land Rover",model: "Land Cruiser",color: "Black",year: "1999",vin: "YT24GDT9R1T1",wholesale_price: "5000")
Inventory.create(make: "Toyota",model: "Corolla",color: "Blue",year: "2008",vin: "465FGE57EY6B6",wholesale_price: "13000")
Inventory.create(make: "Acura",model: "RSX",color: "Blue",year: "2013",vin: "R246HY5W65RD",wholesale_price: "17000")
Inventory.create(make: "Audi",model: "R8",color: "Black",year: "2014",vin: "AUISE54876",wholesale_price: "104500")
Inventory.create(make: "BMW",model: "i580",color: "Red",year: "2014",vin: "AEAJ76R675E4",wholesale_price: "54000")
Inventory.create(make: "Jaguar",model: "F-Type",color: "Green",year: "2015",vin: "JTYYT00554",wholesale_price: "132000")
Inventory.create(make: "Bently",model: "Continental",color: "White",year: "2013",vin: "BELKFAE0045",wholesale_price: "156000")
Inventory.create(make: "Nissan",model: "GT-R",color: "Gray",year: "2015",vin: "NGTR150000001",wholesale_price: "120000")
Inventory.create(make: "BMW",model: "i386",color: "White",year: "2008",vin: "DFEWE65132558",wholesale_price: "24000")
Inventory.create(make: "Suburu",model: "WRX",color: "Blue",year: "2014",vin: "LKFAP9SDF0",wholesale_price: "31000")

Employee.create(first_name: "Aaron",middle_initial: "",last_name: "Lozano",employee_class: "Owner")
Employee.create(first_name: "Anthony",middle_initial: "P",last_name: "Wojcik",employee_class: "Finance Manager")
Employee.create(first_name: "Vanessa",middle_initial: "M",last_name: "Bernal",employee_class: "Inventory Manager")
Employee.create(first_name: "Diego",middle_initial: "",last_name: "Manrique",employee_class: "Sales Manager")
Employee.create(first_name: "Victor",middle_initial: "J",last_name: "Martinez",employee_class: "Sales Person")
Employee.create(first_name: "Gabriel",middle_initial: "E",last_name: "Gonzales",employee_class: "Sales Person")
Employee.create(first_name: "Deborah",middle_initial: "",last_name: "Sticky",employee_class: "Sales Person")
Employee.create(first_name: "Walter",middle_initial: "",last_name: "White",employee_class: "Sales Person")
Employee.create(first_name: "Jennifer",middle_initial: "",last_name: "Johnson",employee_class: "Sales Person")
Employee.create(first_name: "",middle_initial: "",last_name: "",employee_class: "")

Customer.create(employee_rep: "Vanessa M Bernal",first_name: "John",middle_initial: "",last_name: "Johnson",customer_status: "Prospective",customer_address: "960 Tony Alley",customer_city: "Houston",customer_zip: "77058",customer_state: "Texas")
Customer.create(employee_rep: "Victor J Martinez",first_name: "Catherine",middle_initial: "",last_name: "Lane",customer_status: "Prospective",customer_address: "79 Continental Hill",customer_city: "Houston",customer_zip: "",customer_state: "Texas")
Customer.create(employee_rep: "Vanessa M Bernal",first_name: "Nancy",middle_initial: "",last_name: "Bishop",customer_status: "Prospective",customer_address: "300 Northport Hill",customer_city: "Lake Charles",customer_zip: "70601",customer_state: "Louisiana")
Customer.create(employee_rep: "Gabriel E Gonzales",first_name: "Linda",middle_initial: "E",last_name: "Kim",customer_status: "Prospective",customer_address: "3627 Claremont Alley",customer_city: "Houston",customer_zip: "",customer_state: "Texas")
Customer.create(employee_rep: "Jennifer Johnson",first_name: "Martha",middle_initial: "",last_name: "Henry",customer_status: "Prospective",customer_address: "6 Welch Court",customer_city: "Katy",customer_zip: "7749",customer_state: "Texas")
Customer.create(employee_rep: "Jennifer Johnson",first_name: "Jimmy",middle_initial: "C",last_name: "Willis",customer_status: "Prospective",customer_address: "748 Loomis Crossing",customer_city: "Houston",customer_zip: "77089",customer_state: "Texas")
Customer.create(employee_rep: "Jennifer Johnson",first_name: "Keith",middle_initial: "H",last_name: "Jacobs",customer_status: "Prospective",customer_address: "4062 Michigan Place",customer_city: "Lake Charles",customer_zip: "70602",customer_state: "Louisiana")
Customer.create(employee_rep: "Deborah Sticky",first_name: "Beverly",middle_initial: "",last_name: "Harper",customer_status: "Prospective",customer_address: "496 Burning Wood Center",customer_city: "Katy",customer_zip: "77450",customer_state: "Texas")
Customer.create(employee_rep: "Gabriel E Gonzales",first_name: "Alice",middle_initial: "E",last_name: "Hughes",customer_status: "Prospective",customer_address: "8470 Rowland Trail",customer_city: "Houston",customer_zip: "77024",customer_state: "Texas")
Customer.create(employee_rep: "Gabriel E Gonzales",first_name: "Ruth",middle_initial: "",last_name: "Green",customer_status: "Prospective",customer_address: "905 Erie Drive",customer_city: "Houston",customer_zip: "77021",customer_state: "Texas")
Customer.create(employee_rep: "Vanessa M Bernal",first_name: "Richard",middle_initial: "I",last_name: "Matthews",customer_status: "Prospective",customer_address: "712 Killdeer Place",customer_city: "Houston",customer_zip: "77069",customer_state: "Texas")
Customer.create(employee_rep: "Deborah Sticky",first_name: "Walter",middle_initial: "",last_name: "Kelley",customer_status: "Prospective",customer_address: "325 Rieder Terrace",customer_city: "Houston",customer_zip: "77035",customer_state: "Texas")
Customer.create(employee_rep: "Vanessa M Bernal",first_name: "Charles",middle_initial: "O",last_name: "Porter",customer_status: "Prospective",customer_address: "80 Declaration Parkway",customer_city: "Katy",customer_zip: "77491",customer_state: "Texas")
Customer.create(employee_rep: "Jennifer Johnson",first_name: "Clarence",middle_initial: "P",last_name: "Gonzales",customer_status: "Prospective",customer_address: "59 Katie Crossing",customer_city: "Baytown",customer_zip: "77257",customer_state: "Texas")
Customer.create(employee_rep: "Vanessa M Bernal",first_name: "Robin",middle_initial: "Q",last_name: "Bailey",customer_status: "Prospective",customer_address: "647 Tennyson Terrace",customer_city: "Houston",customer_zip: "77045",customer_state: "Texas")
Customer.create(employee_rep: "Victor J Martinez",first_name: "Norma",middle_initial: "V",last_name: "Ferguson",customer_status: "Prospective",customer_address: "720 Old Gate Parkway",customer_city: "Lake Charles",customer_zip: "70603",customer_state: "Louisiana")
Customer.create(employee_rep: "Walter White",first_name: "Linda",middle_initial: "",last_name: "Hart",customer_status: "Prospective",customer_address: "9 Kensington Crossing",customer_city: "Houston",customer_zip: "77099",customer_state: "Texas")
Customer.create(employee_rep: "Victor J Martinez",first_name: "Jonathan",middle_initial: "M",last_name: "Reid",customer_status: "Prospective",customer_address: "518 Annie Plaza",customer_city: "Katy",customer_zip: "77492",customer_state: "Texas")
Customer.create(employee_rep: "Gabriel E Gonzales",first_name: "Sara",middle_initial: "S",last_name: "Ross",customer_status: "Prospective",customer_address: "3754 Amoth Terrace",customer_city: "Houston",customer_zip: "77002",customer_state: "Texas")
Customer.create(employee_rep: "Walter White",first_name: "Harold",middle_initial: "",last_name: "Richards",customer_status: "Prospective",customer_address: "38186 Jenna Way",customer_city: "Lake Charles",customer_zip: "70601",customer_state: "Louisiana")
Customer.create(employee_rep: "Gabriel E Gonzales",first_name: "Jonathan",middle_initial: "I",last_name: "Zimmerman",customer_status: "Prospective",customer_address: "6969 Erado Crossing",customer_city: "Katy",customer_zip: "77450",customer_state: "Texas")
Customer.create(employee_rep: "Victor J Martinez",first_name: "",middle_initial: "",last_name: "",customer_status: "",customer_address: "",customer_city: "",customer_zip: "",customer_state: "")